{
	"name": "PD2 AutoUpdate Tutorial",
	"blt_version" : "2",
	"color" : "111 57 76",
	"description" : "",
    "author" : "LR_Daring",
    "contact" : "",
    "version" : 3,
    "updates" : [
       {
        "identifier" : "pd2_autoupdate_tutorial",
        "host": {
            "meta": "https://gitlab.com/LeftOwlRight/pd2autoupdate-tutorial/-/raw/main/meta.json"
                }
        }
    ],
	"hooks": [

		{
			"hook_id" : "lib/network/handlers/unitnetworkhandler",
			"script_path" : "empty.lua"
		}
	]
}
